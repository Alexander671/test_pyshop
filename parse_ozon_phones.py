import time
import logging
from collections import Counter

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC

import undetected_chromedriver as uc


class SmartScraper:
    scrupping_url = 'https://www.ozon.ru/category/smartfony-15502/?sorting=rating'
    amount = 100

    def __init__(self):
        self.os = []
        self.__page = 0
        self.__counter = 0
        self.driver_links = uc.Chrome(use_subprocess=True, headless=False)
        self.driver_product_page = uc.Chrome(use_subprocess=True, headless=False)

    @property
    def url(self):
        self.__page += 1
        return f"{self.scrupping_url}&page={self.__page}"


    def get_pages_links(self):
        while self.__counter != self.amount:

            self.driver_links.get(self.url)
            time.sleep(10)

            try:
                elements = self.driver_links.find_elements(uc.By.CSS_SELECTOR, '.vi6 .iv7')
                for element in elements:

                    if self.__counter == self.amount:
                        break

                    link = element.find_element(uc.By.TAG_NAME, 'a').get_attribute('href')
                    yield link

            except Exception as e:
                print(f"Error scraping ids: {e}")

    def retrieve_os(self, link):
        try:
            self.driver_product_page.get(link)
            self.driver_product_page.execute_script("window.scrollTo(0, 3600)")

            parent_element = WebDriverWait(self.driver_product_page, 10).until(
                EC.presence_of_element_located((By.XPATH, "//dl[dt/span[text()='Операционная система']]")))

            os = parent_element.text.split('\n')[1]
            parent_element = parent_element.find_element(By.XPATH, f"//dl[dt/span[text()='Версия {os}']]")

            print('Operation System was added:', parent_element.text.split('\n')[1])
            self.os.append(parent_element.text.split('\n')[1])

            self.__counter += 1

        except:
            print('Product is not smartphone:', link)


def write_to_file(content):
    with open('result.txt', 'w') as file:
        file.write(str(content))


if __name__ == '__main__':
    smart_scraper = SmartScraper()
    for link in smart_scraper.get_pages_links():
        smart_scraper.retrieve_os(link)
    result = Counter(smart_scraper.os)

    for key, value in result.most_common():
        print(f'{key} - {value}')

    write_to_file(str(smart_scraper.os))
